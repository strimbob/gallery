type GalleryType = {
  style?: React.CSSProperties;
  className?: string;
  src?: string;
  htmlElement?: 'Img' | 'div' | 'iframe';
  width: number;
};

type GalleryTypeArray = GalleryType[];
