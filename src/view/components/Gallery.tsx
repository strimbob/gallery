import React, { useCallback, useState, Fragment } from 'react';
import { Controlled as ControlledZoom } from 'react-medium-image-zoom';

type Props = {
  style?: React.CSSProperties | undefined;
  className?: string;
  src?: undefined | string;
  width?: number;
  zoomState: (shouldZoom: boolean) => void;
};
export const Gallery = (props: Props) => {
  const [isZoomed, setIsZoomed] = useState(false);

  const handleImgLoad = useCallback(() => {
    // setIsZoomed(true);
  }, []);

  const handleZoomChange = useCallback((shouldZoom) => {
    setIsZoomed(shouldZoom);
    props.zoomState(shouldZoom);
  }, []);

  return (
    <ControlledZoom
      transitionDuration={0}
      isZoomed={isZoomed}
      onZoomChange={handleZoomChange}
      wrapStyle={props.style}
    >
      <img
        className={props.className}
        alt=""
        onLoad={handleImgLoad}
        src={props.src}
      />
    </ControlledZoom>
  );
};

export const GalleryMain = (props: { items: GalleryTypeArray }) => {
  const { items } = props;
  const [isZoomed, setIsZoomed] = useState(false);

  const Items = items.map(
    (item: GalleryType, index: number): React.ReactChild | null => {
      const { style, htmlElement, src, ...reset } = item;
      if (!htmlElement) return null;
      return (
        <Fragment key={index}>
          <Gallery
            zoomState={(_isZoomed) => setIsZoomed(_isZoomed)}
            style={style}
            className={'gallery-item'}
            src={src}
          />
        </Fragment>
      );
    }
  );

  return <div className={isZoomed ? '' : 'gallery-wrapper'}>{Items}</div>;
};
