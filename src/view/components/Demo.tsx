import React, { Fragment, useState } from 'react';
import _images from '../1-Data/images.json';
import { GalleryMain } from './Gallery';

const images = _images.images as GalleryTypeArray;

export const Demo = () => {
  return (
    <>
      <GalleryMain items={images} />
    </>
  );
};
