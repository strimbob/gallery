import { useEffect, useCallback, useState } from 'react';

export const useResize = (callback: () => void) => {
  useEffect(() => {
    window.addEventListener('resize', callback);
    return () => {
      window.removeEventListener('resize', callback);
    };
  }, [callback]);
};

export const useRefSize = (ref: React.RefObject<any>, trigger?: any) => {
  const [width, setWidth] = useState(0);
  const [height, setheight] = useState(0);

  const resizeHandler = useCallback(() => {
    const { current } = ref;
    if (current) {
      setWidth(current.clientWidth);
      setheight(current.clientHeight);
    }
  }, [ref]);

  useEffect(() => {
    resizeHandler();
  }, [trigger, resizeHandler]);

  useResize(resizeHandler);
  return { width, height };
};
